/*

  Задание:
    Написать конструктор используя паттерн композиции.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
import Fly from './modules/fly.js';

import Hunt from './modules/hunt.js';

import Swim from './modules/swim.js';

const Duck = (name, flySpeed) => {
  let state = {
    name,
    flySpeed: Number(flySpeed),
  }
  return Object.assign(
    {},
    state,
    Fly(state),
    Swim(state),
  );
};
const Donald = Duck('Donald', 10);
Donald.getSpeed();
Donald.swimFromShark(30);
Donald.swimFromShark(50);


