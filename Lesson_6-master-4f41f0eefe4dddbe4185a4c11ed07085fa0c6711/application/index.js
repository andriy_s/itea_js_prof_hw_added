// Точка входа в наше приложение

import Singleton from './singleton/';
import Facade from './facade';

import work1 from '../classworks/objectfreeze.js';
import work2 from '../classworks/singleton.js';

// let universe = {
//     infinity: Infinity,
//     good: ['cats', 'love', 'rock-n-roll'],
//     evil: {
//       bonuses: ['cookies', 'great look']
//     }
//   };

// console.log(universe);
// let result = work1(universe);
// console.log(result);

// result.good.push('javascript'); // true
// result.something = 'Wow!'; // false
// result.evil.humans = [];   // false

console.log(work2);
console.log(work2.getsatisfaction());

/*
  Обратите внимание что в среде разработки Singleton / singleton
  это один файл и ошибки из-за реестра не будет, но в продакшене
  это может дать ошибку, потому что например на CentOS такие импотры
  уже не отрабатывают и еррорят.

  + в файлах можно не указывать .js
  + можно подключать файлы с названием index.js обращаясь напрямую к папке
  напрмер если нужно подключить файл ./singleton/index.js,
  то можно зададь в url просто ./singleton
*/

/*
  Партерны можно поделить на три большие группы:
  - Порождающие
  - Структурные
  - Поведенческие

*/

  // Singleton();
  // Facade();
  // work1();

  