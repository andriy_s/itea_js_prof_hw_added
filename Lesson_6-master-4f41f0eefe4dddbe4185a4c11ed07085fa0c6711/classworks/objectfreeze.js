/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // true
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

  // let x = Object.getOwnPropertyNames(universe);
  // console.log(x);

  const DeepFreeze = (unfreezed) => {
    // let result = Object.freeze(unfreezed);
    let freeze = (obj) => {

      Object.freeze(obj);

      Object.getOwnPropertyNames(obj).map( item => {

        let prop = obj(item);
        if (typeof prop == 'object' && prop !== null){
        
          freeze(prop);
      
        }

      });

    }  

      freeze(unfreezed);
      return unfreezed;
    
  }

export default DeepFreeze;