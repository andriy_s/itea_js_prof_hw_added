/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
            
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

  const _obj = {
    laws: [],
    budget: 1000000,
    citizensSatisfactions: 0,
  };

  const singleton = {

    addLaw: (id, name, description) => {
      _obj.laws.push({id, name, description});
      _obj.citizensSatisfactions -= 10;
    },
    addAllLaws: () => _obj.laws,
    getLaw: (id) => _obj.laws.filter(law => law.id === law),
    getsatisfaction: () => _obj.citizensSatisfactions,
    getbudget: () => _obj.budget,
    celebrate: () => {_obj.budget -= 50000; _obj.citizensSatisfactions += 5} 
      
  }

export default singleton;