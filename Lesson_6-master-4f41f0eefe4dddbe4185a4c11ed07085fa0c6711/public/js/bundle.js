/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/facade/api.js":
/*!***********************************!*\
  !*** ./application/facade/api.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst api = {\n  fethchedData: null,\n  status: {\n    fetched: false,\n    fetching: true\n  },\n  fetchData: () => {\n    let url = 'https://next.json-generator.com/api/json/get/V1st09BFE';\n\n    api.status.fetched = false;\n    api.status.fetching = true;\n\n    const returnGlobalData = () => api.fethchedData;\n    return fetch(url)\n      .then( res => res.json() )\n      .then( res => {\n        api.status.fetched = true;\n        api.status.fetching = false;\n        api.fethchedData = res;\n        console.log(api);\n        return res;\n      });\n  },\n  getList: () => {\n    if( api.fethchedData !== null){\n      api.fethchedData.map( item => console.log(item.company) );\n    } else {\n      console.error('Data not fetched');\n    }\n  },\n  getFetchStatus: () => {\n    return api.status;\n  },\n  getUser: id => {\n    return api.fethchedData[id];\n  }\n\n\n\n\n\n};\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (api);\n\n\n//# sourceURL=webpack:///./application/facade/api.js?");

/***/ }),

/***/ "./application/facade/index.js":
/*!*************************************!*\
  !*** ./application/facade/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./api */ \"./application/facade/api.js\");\n\n\nconst FacadeDemo = () => {\n\n  /*\n    Фасад — это структурный паттерн проектирования,\n    который предоставляет простой интерфейс\n    к сложной системе классов, библиотеке или фреймворку.\n\n    https://refactoring.guru/ru/design-patterns/facade\n  */\n  const myFunc = ( data ) => {\n    console.log( 'myFunc', data );\n    _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getList();\n    console.log(\n       'fetch status', _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getFetchStatus(),\n       'user', _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getUser(0)\n      );\n  };\n\n  _api__WEBPACK_IMPORTED_MODULE_0__[\"default\"].fetchData().then( myFunc );\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (FacadeDemo);\n\n\n//# sourceURL=webpack:///./application/facade/index.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _singleton___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleton/ */ \"./application/singleton/index.js\");\n/* harmony import */ var _facade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./facade */ \"./application/facade/index.js\");\n/* harmony import */ var _classworks_objectfreeze_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classworks/objectfreeze.js */ \"./classworks/objectfreeze.js\");\n/* harmony import */ var _classworks_singleton_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../classworks/singleton.js */ \"./classworks/singleton.js\");\n// Точка входа в наше приложение\n\n\n\n\n\n\n\n// let universe = {\n//     infinity: Infinity,\n//     good: ['cats', 'love', 'rock-n-roll'],\n//     evil: {\n//       bonuses: ['cookies', 'great look']\n//     }\n//   };\n\n// console.log(universe);\n// let result = work1(universe);\n// console.log(result);\n\n// result.good.push('javascript'); // true\n// result.something = 'Wow!'; // false\n// result.evil.humans = [];   // false\n\nconsole.log(_classworks_singleton_js__WEBPACK_IMPORTED_MODULE_3__[\"default\"]);\nconsole.log(_classworks_singleton_js__WEBPACK_IMPORTED_MODULE_3__[\"default\"].getsatisfaction());\n\n/*\n  Обратите внимание что в среде разработки Singleton / singleton\n  это один файл и ошибки из-за реестра не будет, но в продакшене\n  это может дать ошибку, потому что например на CentOS такие импотры\n  уже не отрабатывают и еррорят.\n\n  + в файлах можно не указывать .js\n  + можно подключать файлы с названием index.js обращаясь напрямую к папке\n  напрмер если нужно подключить файл ./singleton/index.js,\n  то можно зададь в url просто ./singleton\n*/\n\n/*\n  Партерны можно поделить на три большие группы:\n  - Порождающие\n  - Структурные\n  - Поведенческие\n\n*/\n\n  // Singleton();\n  // Facade();\n  // work1();\n\n  \n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/singleton/index.js":
/*!****************************************!*\
  !*** ./application/singleton/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _object_freeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./object_freeze */ \"./application/singleton/object_freeze.js\");\n/* harmony import */ var _old_singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./old-singleton */ \"./application/singleton/old-singleton.js\");\n/* harmony import */ var _new_singleton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-singleton */ \"./application/singleton/new-singleton.js\");\n\n\n\n// FreezeDemo();\n\nconst SingletonDemo = () => {\n  /*\n    Singleton (он же одиночка)— это паттерн проектирования,\n    который гарантирует, что у класса есть только один экземпляр,\n    и предоставляет к нему глобальную точку доступа.\n\n    Про паттерн: https://refactoring.guru/ru/design-patterns/singleton\n  */\n\n  // Посмотрим на две реализаци старую - до ES6\n  oldSingletonDemo();\n  // И новую\n  newSingetonDemo();\n};\n\nconst oldSingletonDemo = () => {\n    // Смотрим реализацию в файле old-singleton.js\n\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 0, language: 'js'});\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 1, language: 'phyton'});\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 2, language: 'php'});\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 3, language: 'ruby'});\n\n    console.log('OldSingleton', _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n    let myLang = _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(0);\n    console.log('OldSingleton -> myLang', myLang);\n};\n\nconst newSingetonDemo = () => {\n  // Как и все в js в 2017-18 меньше, быстрее, чище!\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 0, language: 'js'});\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 1, language: 'phyton'});\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 2, language: 'php'});\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 3, language: 'ruby'});\n\n  console.log('NewSingleton', _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n  let myLang = _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].get(1);\n  console.log('NewSingleton -> myLang', myLang);\n\n  /*\n    Демо усложнить синглтон\n  */\n};\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (SingletonDemo);\n\n\n//# sourceURL=webpack:///./application/singleton/index.js?");

/***/ }),

/***/ "./application/singleton/new-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/new-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\n// Так как является константой, не может быть измененно\nconst _data = [];\n\n// Создаем обьект и методы\nconst Store = {\n  add: item => _data.push(item),\n  get: id => _data.find( d=> d.id === id ),\n};\n// Замораживаем\nObject.freeze(Store);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Store);\n\n\n//# sourceURL=webpack:///./application/singleton/new-singleton.js?");

/***/ }),

/***/ "./application/singleton/object_freeze.js":
/*!************************************************!*\
  !*** ./application/singleton/object_freeze.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst ObjectFreezeDemo = () => {\n  /*\n    Разберемся вначале с Object.freeze. ->\n    https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze\n  */\n\n  let myObj = {\n    name: 'Dexter',\n    prop: () => {\n      console.log(`${undefined.name} woohoo!`);\n    }\n  };\n  console.log( myObj );\n  myObj.name = \"Debra\";\n  console.log( myObj );\n\n  /*\n    Заморозка обьекта, это необратимый процесс.\n    Единожды заомроженный обьект уже не может быть разморожен!\n    Заморозим обьект\n  */\n  let frozen = Object.freeze(myObj);\n  //Попробуем изменить или добавить значение\n  // frozen.name = \"Vince\";\n  // frozen.secondName = 'Morgan';\n\n  // Проверим сам обьект и его идентичность с тем,\n  // что мы создали в самом начале функции\n  // console.log( 'frozen', frozen, 'equal?', frozen === myObj);\n\n  /*\n    Так же, метод работает для массивов\n  */\n  let frozenArray = Object.freeze(['froze', 'inside', 'of', 'array']);\n\n  // Попробуем добавить новое значение\n  // frozenArray[0] = 'Noooooo!';\n\n  // Попробуем использовать методы\n  let sliceOfColdAndSadArray = frozenArray.slice(0, 1);\n  sliceOfColdAndSadArray.map( item => console.log( item ) );\n  console.log(frozenArray, sliceOfColdAndSadArray);\n\n  // Метоы для проверки\n  // Object.isFrozen( obj ) -> Вернет true если обьект заморожен\n  console.log(\n    'is myObj frozen?', Object.isFrozen( myObj ),\n    '\\nis frozen frozen?', Object.isFrozen( frozen ),\n    '\\nis array frozen', Object.isFrozen( frozenArray )\n  );\n\n  /*\n    Заморозка в обьектах является не глубокой\n  */\n\n  let universe = {\n    infinity: Infinity,\n    good: ['cats', 'love', 'rock-n-roll'],\n    evil: {\n      bonuses: ['cookies', 'great look']\n    }\n  };\n  let x = Object.getOwnPropertyNames(universe);\n  console.log(x);\n  // let frozenUniverse = Object.freeze(universe);\n      // frozenUniverse.humans = [];\n      // frozenUniverse.evil.humans = [];\n\n      // console.log(frozenUniverse);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObjectFreezeDemo);\n\n\n//# sourceURL=webpack:///./application/singleton/object_freeze.js?");

/***/ }),

/***/ "./application/singleton/old-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/old-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst OldSingleton = ( function(){\n var _data = [];\n\n function add(item){\n   _data.push(item);\n }\n\n function get(id){\n   return _data.find((d) => {\n       return d.id === id;\n   });\n }\n\n return {\n   add: add,\n   get: get\n };\n})();\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (OldSingleton);\n\n\n//# sourceURL=webpack:///./application/singleton/old-singleton.js?");

/***/ }),

/***/ "./classworks/objectfreeze.js":
/*!************************************!*\
  !*** ./classworks/objectfreeze.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: написать функцию, для глубокой заморозки обьектов.\n\n  Обьект для работы:\n  let universe = {\n    infinity: Infinity,\n    good: ['cats', 'love', 'rock-n-roll'],\n    evil: {\n      bonuses: ['cookies', 'great look']\n    }\n  };\n\n  frozenUniverse.evil.humans = []; -> Не должен отработать.\n\n  Методы для работы:\n  1. Object.getOwnPropertyNames(obj);\n      -> Получаем имена свойств из объекта obj в виде массива\n\n  2. Проверка через typeof на обьект или !== null\n  if (typeof prop == 'object' && prop !== null){...}\n\n  Тестирование:\n\n  let FarGalaxy = DeepFreeze(universe);\n      FarGalaxy.good.push('javascript'); // true\n      FarGalaxy.something = 'Wow!'; // false\n      FarGalaxy.evil.humans = [];   // false\n\n*/\n\n  // let x = Object.getOwnPropertyNames(universe);\n  // console.log(x);\n\n  const DeepFreeze = (unfreezed) => {\n    // let result = Object.freeze(unfreezed);\n    let freeze = (obj) => {\n\n      Object.freeze(obj);\n\n      Object.getOwnPropertyNames(obj).map( item => {\n\n        let prop = obj(item);\n        if (typeof prop == 'object' && prop !== null){\n        \n          freeze(prop);\n      \n        }\n\n      });\n\n    }  \n\n      freeze(unfreezed);\n      return unfreezed;\n    \n  }\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (DeepFreeze);\n\n//# sourceURL=webpack:///./classworks/objectfreeze.js?");

/***/ }),

/***/ "./classworks/singleton.js":
/*!*********************************!*\
  !*** ./classworks/singleton.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание:\n\n    Написать синглтон, который будет создавать обьект government\n\n    Данные:\n    {\n        laws: [],\n        budget: 1000000\n        citizensSatisfactions: 0,\n    }\n\n    У этого обьекта будут методы:\n      .добавитьЗакон({id, name, description})\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\n\n      .читатькКонституцию -> Вывести все законы на экран\n      .читатьЗакон(ид)\n\n      .показатьУровеньДовольства()\n      .показатьБюджет()\n            \n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\n\n\n*/\n\n  const _obj = {\n    laws: [],\n    budget: 1000000,\n    citizensSatisfactions: 0,\n  };\n\n  const singleton = {\n\n    addLaw: (id, name, description) => {\n      _obj.laws.push({id, name, description});\n      _obj.citizensSatisfactions -= 10;\n    },\n    addAllLaws: () => _obj.laws,\n    getLaw: (id) => _obj.laws.filter(law => law.id === law),\n    getsatisfaction: () => _obj.citizensSatisfactions,\n    getbudget: () => _obj.budget,\n    celebrate: () => {_obj.budget -= 50000; _obj.citizensSatisfactions += 5} \n      \n  }\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (singleton);\n\n//# sourceURL=webpack:///./classworks/singleton.js?");

/***/ })

/******/ });