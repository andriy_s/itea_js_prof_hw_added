import Base from './basicUsage';
import {FunctionDecorator, es7Decorator} from './es7_decorator';
import Hoc from '../features/hoc';
import Define from '../features/define_property';
/*
  Декоратор — это структурный паттерн проектирования,
  который позволяет динамически добавлять объектам новую
  функциональность, оборачивая их в полезные «обёртки».

  https://refactoring.guru/ru/design-patterns/decorator
*/

const DecoratorDemo = () => {

  // console.log( 'DECORATOR AS DESIGN PATTERN DEMO!');
  // Base();
  // console.log( '- - - - - - - - - - - -');
  // Hoc();
  // Define();
  // console.log( '- - - - - - - - - - - -');
  FunctionDecorator();

  class Testo {

  }
  // console.log( new Testo('Grecha') );

  es7Decorator();
};

export default DecoratorDemo;
