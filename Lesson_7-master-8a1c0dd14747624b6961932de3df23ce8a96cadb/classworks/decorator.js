/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

const BeachParty = () => {

  console.log( 'your code ');

  let coolers = [
        {name: "icecream", temperatureCoolRate: -5},
        {name: "water", temperatureCoolRate: -2},
        {name: "juice", temperatureCoolRate: -3},
      ]

  function Human( name ){

    this.name = name,
    this.currentTemperature = 20,
    this.minTemperature = 0,
    this.maxTemperature = 50

    console.log(`new Human ${this.name} arrived!`);
  }

  Human.prototype.ChangeTemperature = function ChangeTemperature( changeValue ) {

    console.log(
          'current', this.currentTemperature + changeValue,
          'min', this.minTemperature,
          'max', this.maxTemperature
        );

    this.currentTemperature = this.currentTemperature + changeValue;

        if( this.currentTemperature < this.minTemperature ){
          console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
        } 
        else if ( this.currentTemperature > this.maxTemperature ) {
          console.error(`Temperature is to high: ${this.currentTemperature}. ${this.name} burned in tne sun :(`);
        }
        else if ( this.currentTemperature > 30 ){
          this.currentTemperature = this.currentTemperature + coolers[0].temperatureCoolRate;
          console.log(`Temperature was too high, so ${this.name} had eaten an icecream and his/her currentTemperature now is : ${this.currentTemperature}.`);
        }
        else {
          console.log('All is ok.');
        }

  }

  let Morgan = new Human('Morgan');
          Morgan.ChangeTemperature(5);
          Morgan.ChangeTemperature(-21);
          Morgan.ChangeTemperature(33);

}

export default BeachParty;
